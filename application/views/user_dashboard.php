<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Coforge User Panel</title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <style>
            body { background-color: #e9ecef; margin: 0; }
            div { display: block; box-sizing: border-box; }
            .login-box { 
                position: absolute;
                top: 50%;
                left: 50%;
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                width: 300px;
            }
            .form-element { width: 100%; margin-bottom: 10px; box-sizing: border-box; }
            label { font-size: 12px; margin-bottom: 10px; }
            .login-button { margin-top: 30px; }
            .error { font-size: 12px; color: red; }
            .card { background-color: white; box-shadow: 0 0 6px 0px grey; }
            .header { background-color: #484a48; float: left; width: 100%; color: whitesmoke; margin: 0; padding: 10px; }
            .header-user { width: 50%; text-align: left; float: left; }
            .header-user p { margin: 0; }
            .header-links { width: 50%; text-align: right; float: left; }
        </style>
    </head>
  
    <body>
        <div class="header">
            <div class="header-user">
                <p><strong>Current User:</strong> <?php echo $username; ?></p>
            </div>
            <div class="header-links">
                <a href="doLogout" class="btn btn-sm btn-danger">Logout</a>
            </div>
        </div>

        <div class="login-box" style="">
            <h5 style="text-align: center; text-decoration: underline;">Resume Section</h5>
            <br/>
            <div class="card">
                <div class="card-body">
                    <?php echo form_open_multipart('', array('id' => 'formResumeUpload')); ?>
                    <label for="password"><strong>Select File To Upload: </strong><br/>(Allowed Files: pdf/doc/docx, Max Size: 2MB)</label>
                    <input class="form-element" style="font-size: 13px;" type="file" name="file_cv" id="file_cv" placeholder="Password" required="" />
                    <input class="form-element btn btn-primary login-button" type="submit" value="Upload Resume" name="submit-resume" />
                    <?php if(!empty($current_cv_data->u_is_cv_uploaded) && $current_cv_data->u_is_cv_uploaded == 1) { ?>
                    <a href="downloadExistingCV" class="btn btn-success form-element">Download Existing Resume</a>
                    <?php } ?>
                    <?php echo form_close(); ?>
                    <?php if($this->session->flashdata('message')) { ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php } ?>
                    <?php if(!empty($message)) { ?>
                        <div class="alert alert-<?php echo ($upload_status === TRUE) ? "success" : "danger"; ?>">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <?php echo $message; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script>
            $(document).on('submit', '#formResumeUpload', function(e) {
                var file = $('#file_cv');

                if((file)[0].files.length > 0) {
                    const file_size = (file)[0].files.item(0).size;
                    const file_name = (file)[0].files.item(0).name;
                    const file_ext = file_name.split('.')[file_name.split('.').length - 1].toLowerCase();
                    
                    if(file_size <= 0 || file_size > (2*1024*1024) || $.inArray(file_ext, ['pdf', 'docx', 'doc']) == -1) {
                        alert('Please upload a valid file');
                    } else {
                        return true;
                    }
                }

                return false;
            });
        </script>
    </body>
</html>