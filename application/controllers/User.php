<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->helper('common');
        $this->load->library('form_validation');
        $this->load->model('User_Model', 'user');
    }

    private function isUserLoggedIn()
    {
        return $this->session->userdata('is_logged_in') ? TRUE : FALSE;
    }
    
    public function login() 
    {
        if($this->isUserLoggedIn()) {
            redirect('user/dashboard');
        }

        $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE) {
            $this->load->view('user_login');
        } else {
            $username = $this->security->xss_clean($this->input->post('username'));
            $password = $this->security->xss_clean($this->input->post('password'));
            
            $user_data = $this->user->checkUserLogin($username, $password);
            if(!empty($user_data)) {
                $this->session->set_userdata(array('u_id' => $user_data->u_id, 'u_name' => $user_data->u_username, 'is_logged_in' => TRUE));
                redirect('user/dashboard');
            } else {
                $this->session->set_flashdata('message', 'Invalid login credentials');
                redirect('user/login');
            }
        }
    }

    public function dashboard() 
    {
        if(!$this->isUserLoggedIn()) {
            redirect('user/login');
        }
        
        $data['username'] = $this->session->userdata('u_name');

        if($this->input->post('submit-resume')) {
            $res_valid = $this->validateUploadedFile('file_cv');
            
            if(!empty($res_valid['status']) && $res_valid['status'] === TRUE) {
                $this->load->model('AWS_Model', 'aws');
                $res_upd = $this->aws->uploadFileToBucket($_FILES['file_cv']['tmp_name'], (pathinfo($_FILES['file_cv']['name'])['extension']));
                
                $data['upload_status'] = $res_upd['status'];
                $data['message'] = $res_upd['msg'];
            } else {
                $data['upload_status'] = FALSE;
                $data['message'] = $res_valid['msg'];
            }
        }

        $data['current_cv_data'] = $this->user->getExistingCV();

        $this->load->view('user_dashboard', $data);
    }

    public function doLogout()
    {
        $this->session->sess_destroy();
        redirect('user/login');
    }

    private function validateUploadedFile($file)
    {
        $response = array('status' => FALSE, 'msg' => 'Please choose a file to upload');
        
        if(isset($_FILES[$file]) && $_FILES[$file]['size'] > 0) {
            $file_ext = pathinfo($_FILES[$file]['name'])['extension'];
            $file_size = $_FILES[$file]['size'];
            $allowed_file_ext = array('pdf', 'doc', 'docx');

            if(!in_array(strtolower($file_ext), $allowed_file_ext)) {
                $response['msg'] = 'Please upload a valid file type';
                return $response;
            } elseif($file_size > (2 * 1024 * 1024)) {
                $response['msg'] = 'Please upload a file of max 2MB';
                return $response;
            }

            $response['status'] = TRUE;
        }

        return $response;
    }

    public function downloadExistingCV()
    {
        if(!$this->isUserLoggedIn()) {
            redirect('user/login');
        }

        $cv_data = $this->user->getExistingCV();
        if(!empty($cv_data->u_is_cv_uploaded) && $cv_data->u_is_cv_uploaded == 1) {
            $this->load->model('AWS_Model', 'aws');
            $res_down = $this->aws->downloadFileFromBucket($cv_data->u_cv_path_url);
            
            if($res_down['status'] === TRUE) {
                $file_ext = pathinfo($cv_data->u_cv_path_url)['extension'];

                $this->load->helper('download');
                force_download($cv_data->u_cv_path_url, $res_down['file_data']);
            } else {
                $this->session->set_flashdata('message', $res_down['msg']);
                redirect('user/dashboard');    
            }
        } else {
            redirect('user/dashboard');
        }       
    }
}
