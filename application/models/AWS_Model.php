<?php

require 'AWS/aws-autoloader.php';

use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use Aws\S3\Exception\S3Exception;
use Aws\Exception\InvalidRegionException;

class AWS_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function uploadFileToBucket($file, $extention) 
    {
        $response = array('status' => FALSE, 'msg' => 'Something went wrong');
        $upload_file_name = $this->session->userdata('u_id') . '.' . $extention;

        try {
            $s3 = new S3Client([
                'version' => 'latest',
                'region'  => getenv('AWS_REGION'),
                'credentials' => ['key' => getenv('AWS_ACCESS_KEY'), 'secret' => getenv('AWS_ACCESS_SECRET')]
            ]);

            $result = $s3->putObject([
                'Bucket' => getenv('AWS_BUCKET'),
                'Key'    => 'user_resumes/' . $upload_file_name,
                'SourceFile'   => $file
            ]);

            if(!empty($result['@metadata']['statusCode']) && $result['@metadata']['statusCode'] === 200) {
                $response['status'] = TRUE;
                $response['msg'] = 'File uploaded successfully';

                $this->load->model('User_Model', 'user');
                $this->user->updateCVData($upload_file_name);
            }
        } catch (S3Exception $e) {
            log_message('error', trim(preg_replace('/\s+/', ' ', $e->getMessage())));
        } catch (InvalidRegionException $e) {
            log_message('error', trim(preg_replace('/\s+/', ' ', $e->getMessage())));
        } catch (InvalidArgumentException $e) {
            log_message('error', trim(preg_replace('/\s+/', ' ', $e->getMessage())));
        }

        return $response;
    }

    function downloadFileFromBucket($file_name) 
    {
        $response = array('status' => FALSE, 'msg' => 'Something went wrong');

        try {
            $s3 = new S3Client([
                'version' => 'latest',
                'region'  => getenv('AWS_REGION'),
                'credentials' => ['key' => getenv('AWS_ACCESS_KEY'), 'secret' => getenv('AWS_ACCESS_SECRET')]
            ]);

            $result = $s3->getObject([
                'Bucket' => getenv('AWS_BUCKET'),
                'Key'    => 'user_resumes/' . $file_name
            ]);
                
            if(!empty($result['@metadata']['statusCode']) && $result['@metadata']['statusCode'] === 200) {
                $response['status'] = TRUE;
                $response['file_data'] = $result['Body'];
            }
        } catch (S3Exception $e) {
            log_message('error', trim(preg_replace('/\s+/', ' ', $e->getMessage())));
        } catch (InvalidRegionException $e) {
            log_message('error', trim(preg_replace('/\s+/', ' ', $e->getMessage())));
        } catch (InvalidArgumentException $e) {
            log_message('error', trim(preg_replace('/\s+/', ' ', $e->getMessage())));
        }

        return $response;
    }
}